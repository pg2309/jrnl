/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.giancola.jrnl;

/**
 *
 * @author Peter Giancola
 */
public class PeteLib 
{
// Need an integer version.



    //
    // RoundTo(value, size, upDown)
    //
    // Round a value to the nearest multiple of the given size. Works with 
    // positive and negative values, and for fractional size.
    //
    //      upDown = -2 --- Round towards 0             (away from pos/neg infinity)
    //               -1 --- Round down                  (towards neg infinity)
    //                0 --- Round nearest               (closest "size")
    //                1 --- Round up                    (towards pos infinity)
    //                2 --- Round towards infinity      (away from 0)
    //
    public static double RoundTo(double value, double size, int upDown)
    {
        double div, mod;

        // Get the number of even divisions of the given size in the value and the 
        // remainder of the value after removing the number of even divisions
        if (value >= 0.0f)
        {
            div = Math.floor(value / size);
            mod = value - (size * div);
        }
        else
        {
            div = Math.ceil(value / size);
            mod = value - (size * div);
        }

        // If there is a remainder, need to round
        if (mod != 0.0f)
        {

            // Based on request, round nearest or up
            // (round down has been done by "floor()" above)
            switch (upDown)
            {

            // Request is round towards zero
            case -2:
                if ((value > 0.0f) && (div > 0.0f))
                {
                    div--;
                }
                else if ((value < 0.0f) && (div < 0.0f))
                {
                    div++;
                }
                break;

            // Request is round down
            case -1:
                break;

            // Request is round nearest
            case 0:

                // If the remainder is greater than half the "size", add one 
                // more "size" to the value
                if (Math.abs(mod) > (size / 2.0f))
                {
                    if (mod > 0.0f)
                    {
                        div++;
                    }
                    else if (mod < 0.0f)
                    {
                        div--;
                    }
                }
                break;

            // Request is round up
            case 1:
                div++;
                break;

            // Request is round towards infinity (pos or neg)
            case 2:
                if (value > 0.0f)
                {
                    div++;
                }
                else if (value < 0.0f)
                {
                    div--;
                }
            }
        }

        // Calculate the new rounded value and return
        div *= size;
        return div;
    }
}
