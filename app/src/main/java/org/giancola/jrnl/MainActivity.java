package org.giancola.jrnl;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import javax.xml.parsers.ParserConfigurationException;
import org.giancola.statemachine.*;
import org.xml.sax.SAXException;
import static org.giancola.jrnl.PeteLib.RoundTo;

public class MainActivity extends AppCompatActivity 
    implements AdapterView.OnItemSelectedListener, View.OnClickListener
{
    public static String LOGTAG = "Peep";

    private StateMachine m_sm = null;
    private StateMachine m_appsm = null;

//pag/    private Spinner itemSpinner = null;
//pag/    private Spinner typeSpinner = null;
    private Spinner sizeSpinner = null;
    private TextView tv = null;
    private TextView currDateText = null;
    private TextView editDate = null;
    private TextView editTime = null;
    private TextView currStateText = null;
    private EditText itemEditText = null;
    private TextView itemListView = null;
    private EditText typeEditText = null;
    private TextView typeListView = null;

    private Button enterButton = null;
    private Button nowButton = null;

    private Cursor itemCursor;
    private Cursor typeCursor;

    private SQLiteDatabase m_db;
    private MyCursorAdapter cada;
    private SimpleCursorAdapter sca;
    private String currDisplayDate = "";
    private String backupDatabaseName = "";

    private String[] sizeNames = {"", "1", "2", "3", "4", "999"};

    public SQLiteDatabase getdb()
    {
        return m_db;
    }

    public class DateTimeHolder
    {
        private DateTimeHolder(String date, String time) { this.date = date; this.time = time; }
        public String getDate() { return date; }
        public void setDate(String date) { this.date = date; }
        public String getTime() { return time; }
        public void setTime(String time) { this.time = time; }
        private String date;
        private String time;
    }

    // Actions.
    public void MakeNewEntry(String query, Object... params)
    {
        long datetime = 0;
//pag/        long itemnum = itemSpinner.getSelectedItemId() - 1;
//pag/        long typenum = typeSpinner.getSelectedItemId() - 1;
        long sizenum = sizeSpinner.getSelectedItemId();
//pag/        itemCursor.moveToPosition((int)itemnum);
//pag/        typeCursor.moveToPosition((int)typenum);
        String itemstr = itemCursor.getString(1);
        String typestr = typeCursor.getString(1);
        String sizestr = sizeSpinner.getSelectedItem().toString();
        // Readjust IDs to database values.
//pag/        itemnum++;
//pag/        typenum++;

        String dta = editDate.getText().toString();
        String tta = editTime.getText().toString();
        DateTimeHolder dth = new DateTimeHolder(dta, tta);
        Boolean valid = ValidateDateTime(dth);
        if (!valid)
        {
            tv.append("date/time invalid\n");
            return;
        }
        dta = dth.getDate();
        tta = dth.getTime();

        // Store the new event.
        ContentValues values = new ContentValues();
        values.put("date", dta);
        values.put("time", tta);
////////////
// Note:
// Fix this
////////////
        values.put("itemid", 666);//itemnum);
        values.put("typeid", 666);//typenum);
        values.put("size", sizenum);
        m_db.insert("events", null, values);

        // Update the last IDs.
        String maxEventQuery = "update lastids set lastevent=(select max(_id) from events) where _id == 1;";
        m_db.execSQL(maxEventQuery);
    }

    public void MakeNewItem(String query, Object... params)
    {
//pag/        long datetime = 0;
//pag/        long itemnum = itemSpinner.getSelectedItemId() - 1;
//pag/        long typenum = typeSpinner.getSelectedItemId() - 1;
//pag/        long sizenum = sizeSpinner.getSelectedItemId();
//pag/        itemCursor.moveToPosition((int)itemnum);
//pag/        typeCursor.moveToPosition((int)typenum);
//pag/        String itemstr = itemCursor.getString(1);
//pag/        String typestr = typeCursor.getString(1);
//pag/        String sizestr = sizeSpinner.getSelectedItem().toString();
//pag/        // Readjust IDs to database values.
//pag/        itemnum++;
//pag/        typenum++;
//pag/
//pag/        String dta = editDate.getText().toString();
//pag/        String tta = editTime.getText().toString();
//pag/        DateTimeHolder dth = new DateTimeHolder(dta, tta);
//pag/        Boolean valid = ValidateDateTime(dth);
//pag/        if (!valid)
//pag/        {
//pag/            tv.append("date/time invalid\n");
//pag/            return;
//pag/        }
//pag/        dta = dth.getDate();
//pag/        tta = dth.getTime();
//pag/
//pag/        // Store the new event.
//pag/        ContentValues values = new ContentValues();
//pag/        values.put("date", dta);
//pag/        values.put("time", tta);
//pag/        values.put("itemid", itemnum);
//pag/        values.put("typeid", typenum);
//pag/        values.put("size", sizenum);
//pag/        m_db.insert("events", null, values);
//pag/
//pag/        // Update the last IDs.
//pag/        String maxEventQuery = "update lastids set lastevent=(select max(_id) from events) where _id == 1;";
//pag/        m_db.execSQL(maxEventQuery);
    }

    public void MakeNewType(String query, Object... params)
    {
//pag/        long datetime = 0;
//pag/        long itemnum = itemSpinner.getSelectedItemId() - 1;
//pag/        long typenum = typeSpinner.getSelectedItemId() - 1;
//pag/        long sizenum = sizeSpinner.getSelectedItemId();
//pag/        itemCursor.moveToPosition((int)itemnum);
//pag/        typeCursor.moveToPosition((int)typenum);
//pag/        String itemstr = itemCursor.getString(1);
//pag/        String typestr = typeCursor.getString(1);
//pag/        String sizestr = sizeSpinner.getSelectedItem().toString();
//pag/        // Readjust IDs to database values.
//pag/        itemnum++;
//pag/        typenum++;
//pag/
//pag/        String dta = editDate.getText().toString();
//pag/        String tta = editTime.getText().toString();
//pag/        DateTimeHolder dth = new DateTimeHolder(dta, tta);
//pag/        Boolean valid = ValidateDateTime(dth);
//pag/        if (!valid)
//pag/        {
//pag/            tv.append("date/time invalid\n");
//pag/            return;
//pag/        }
//pag/        dta = dth.getDate();
//pag/        tta = dth.getTime();
//pag/
//pag/        // Store the new event.
//pag/        ContentValues values = new ContentValues();
//pag/        values.put("date", dta);
//pag/        values.put("time", tta);
//pag/        values.put("itemid", itemnum);
//pag/        values.put("typeid", typenum);
//pag/        values.put("size", sizenum);
//pag/        m_db.insert("events", null, values);
//pag/
//pag/        // Update the last IDs.
//pag/        String maxEventQuery = "update lastids set lastevent=(select max(_id) from events) where _id == 1;";
//pag/        m_db.execSQL(maxEventQuery);
    }

    public void OpenItemView(String query, Object... params)
    {
        itemListView.setText("");
        itemListView.bringToFront();
        itemListView.setVisibility(itemListView.VISIBLE);
    }

    public void PopulateItemView(String query, Object... params)
    {
        itemListView.setText("");
        String item = itemEditText.getText().toString();
        String itemquery = String.format("select name from items where name like '%%%s%%';", item);
        Cursor c = m_db.rawQuery(itemquery, null);
        if (c.moveToFirst())
        {
            while (!c.isAfterLast())
            {
                String name = c.getString(0);
                itemListView.append(name);
                itemListView.append("\n");
                c.moveToNext();
            }
        }
    }

    public void CloseItemView(String query, Object... params)
    {
        itemListView.setVisibility(itemListView.GONE);
    }

    public void OpenTypeView(String query, Object... params)
    {
        typeListView.setText("");
        typeListView.bringToFront();
        typeListView.setVisibility(typeListView.VISIBLE);
    }

    public void PopulateTypeView(String query, Object... params)
    {
        typeListView.setText("");
        String type = typeEditText.getText().toString();
        String typequery = String.format("select name from types where name like '%%%s%%';", type);
        Cursor c = m_db.rawQuery(typequery, null);
        if (c.moveToFirst())
        {
            while (!c.isAfterLast())
            {
                String name = c.getString(0);
                typeListView.append(name);
                typeListView.append("\n");
                c.moveToNext();
            }
        }
    }

    public void CloseTypeView(String query, Object... params)
    {
        typeListView.setVisibility(typeListView.GONE);
    }

    public void SetCurrDateYesterday(String query, Object... params)
    {
        Log.d(LOGTAG, "SetCurrDateYesterday");
        LocalDate curr = LocalDate.parse(currDisplayDate).minusDays(1);
        currDisplayDate = curr.toString();
        currDateText.setText(currDisplayDate);
    }

    public void SetCurrDateToday(String query, Object... params)
    {
        Calendar c = Calendar.getInstance();
        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);
        // Calendar returns month with zero-origin, adjust.
        ++m;
        currDisplayDate = String.format(Locale.US, "%04d-%02d-%02d", y, m, d);
        currDateText.setText(currDisplayDate);
    }

    public void SetCurrDateTomorrow(String query, Object... params)
    {
        Log.d(LOGTAG, "SetCurrDateTomorrow");
        LocalDate curr = LocalDate.parse(currDisplayDate).plusDays(1);
        currDisplayDate = curr.toString();
        currDateText.setText(currDisplayDate);
    }

    public void ShowCurrentEvents(String query, Object... params)
    {
        String eventQuery = "select e.time, i.name, t.name, e.size " +
            "from events e, items i, types t " +
            "where (e.itemid == i._id) and (e.typeid == t._id) and (e.date == '%s')" +
            "order by e.date, e.time;";
        // Filter on the current display date.

        String currdtt = "";
        String curritem = "";
        eventQuery = String.format(Locale.US, eventQuery, currDisplayDate);
        Cursor eventCursor = m_db.rawQuery(eventQuery, null);
        eventCursor.moveToFirst();
        while (!eventCursor.isAfterLast())
        {
            String dtt = eventCursor.getString(0).substring(0, 5);
            String item = eventCursor.getString(1);
            String type = eventCursor.getString(2);
            int size = eventCursor.getInt(3);

            String result = "";
            // If the new item is not on the current date, make a new line
            // and use new date as current.
            // Also make a new line if the new item is an "output".
            if (!dtt.equals(currdtt) || 
                item.startsWith("-") || curritem.startsWith("-"))
            {
                if (!currdtt.equals(""))
                {
                    result += "\n";
                }
                result += dtt + " ";
                currdtt = dtt;
            }
            else
            {
                // The is the same date, just append.
                result += " | ";
            }
            curritem = item;

            result += item;
            if (!type.equals(" "))
            {
                result += ": " + type;
            }
            if (size > 0)
            {
                result += String.format(Locale.US, " %d", size);
            }
            tv.append(result);

            eventCursor.moveToNext();
        }
        eventCursor.close();
    }

    public void VerifyTables(String query, Object... params)
    {
        String errors = "";
        String q = "select * from lastids where " +
                        "_id == 1 and " +
                        "lasttype == (select max(_id) from types) and " +
                        "lastitem == (select max(_id) from items) and " +
                        "lastevent == (select max(_id) from events) and " +
                        "lastsize == (select max(_id) from sizes);";
        Cursor c = m_db.rawQuery(q, null);

        if (c.getCount() == 0)
        {
            errors += "Database IDs invalid.\n";
        }

        // Need to check the events table for valid IDs in items, types, and
        // sizes.


        if (errors.length() > 0)
        {
            Toast.makeText(this, String.format("error: %s", errors), Toast.LENGTH_LONG).show();
        }
    }

    public void AddOnFocusChangeListener(View theView, String viewName)
    {
        final String fcIn = "FocusChange" + viewName + "In";
        final String fcOut = "FocusChange" + viewName + "Out";
        theView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) 
            { 
                if (hasFocus) 
                {
                    Log.d(LOGTAG, "onFocusChange: " + fcIn);
                    execute(fcIn, m_sm); 
                }
                else 
                { 
                    Log.d(LOGTAG, "onFocusChange: " + fcOut);
                    execute(fcOut, m_sm); 
                }
            }
        });
    }

    public void CreateViews(String query, Object... params)
    {
        setContentView(R.layout.activity_main);
        itemListView = (TextView)findViewById(R.id.itemListView);
        itemListView.setVisibility(itemListView.GONE);
        AddOnFocusChangeListener(itemListView, "ItemListView");
        typeListView = (TextView)findViewById(R.id.typeListView);
        typeListView.setVisibility(typeListView.GONE);
        AddOnFocusChangeListener(typeListView, "TypeListView");
        tv = (TextView)findViewById(R.id.textView2);
        tv.setMovementMethod(new ScrollingMovementMethod());
        AddOnFocusChangeListener(tv, "TextView");
        editDate = (TextView)findViewById(R.id.editDate);
        AddOnFocusChangeListener(editDate, "EditDate");
        editTime = (TextView)findViewById(R.id.editTime);
        AddOnFocusChangeListener(editTime, "EditTime");
        currDateText = (TextView)findViewById(R.id.currDateText);
        AddOnFocusChangeListener(currDateText, "CurrDateText");
        currStateText = (TextView)findViewById(R.id.currStateText);
        AddOnFocusChangeListener(currStateText, "CurrStateText");
        enterButton = (Button)findViewById(R.id.enterButton);
        AddOnFocusChangeListener(enterButton, "EnterButton");
        nowButton = (Button)findViewById(R.id.nowButton);
        AddOnFocusChangeListener(nowButton, "NowButton");
        itemEditText = (EditText)findViewById(R.id.itemEditText);
        AddOnFocusChangeListener(itemEditText, "ItemEditText");
        itemEditText.setText("");
        itemEditText.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { execute("TextChangedItemEditText", m_sm); }
        });
        typeEditText = (EditText)findViewById(R.id.typeEditText);
        AddOnFocusChangeListener(typeEditText, "TypeEditText");
        typeEditText.setText("");
        typeEditText.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { execute("TextChangedTypeEditText", m_sm); }
        });

//pag/        itemSpinner = (Spinner)findViewById(R.id.itemSpinner);
//pag/        typeSpinner = (Spinner)findViewById(R.id.typeSpinner);
//pag/        AddOnFocusChangeListener(typeSpinner, "TypeSpinner");
        sizeSpinner = (Spinner)findViewById(R.id.sizeSpinner);
        AddOnFocusChangeListener(sizeSpinner, "SizeSpinner");

        Journal journal = new Journal(this, "jrnl.db", R.raw.jrnltableinit);
        m_db = journal.getWritableDatabase();
    }




// Modeless dialog
//pag/if (progressDialog == null) 
//pag/{
//pag/    progressDialog = new Dialog(activityRequestingProgressDialog);
//pag/    progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//pag/    progressDialog.setContentView(R.layout.progress_upload);
//pag/    progressBar = (ProgressBar) progressDialog.findViewById(R.id.progressBar);
//pag/    progressText = (TextView) progressDialog.findViewById(R.id.progressText);
//pag/    progressText.setText("0 %");
//pag/    progressText.setTextSize(18);
//pag/    Button buttonCancel = (Button) progressDialog.findViewById(R.id.btnCancel);
//pag/    buttonCancel.setOnClickListener(new View.OnClickListener() {
//pag/        public void onClick(View view) {
//pag/            cancelProgressDialog();
//pag/            stopUpload("Upload cancelled.");
//pag/        }
//pag/    });
//pag/    Window window = progressDialog.getWindow();
//pag/    window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
//pag/            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
//pag/    window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//pag/    window.setGravity(Gravity.BOTTOM);
//pag/    progressDialog.show();
//pag/}
//pag/
//pag/progressText.setText(text);
//pag/progressBar.setProgress(percent);





    public void WriteState(String query, Object... params)
    {
        String msg = "";
        if (m_sm == null)
        {
            msg = "m_sm is null\n";
        }
        else if (m_appsm == null)
        {
            msg = "m_appsm is null\n";
        }
        else
        {
            msg = String.format(Locale.US, "%s--%s", m_sm.getCurrState().getName(),
                m_appsm.getCurrState().getName());
        }
        currStateText.setText(msg);
    }

    public void ClearEntry(String query, Object... params)
    {
        tv.setText("");
    }

    public void SetNew(String query, Object... params)
    {
//pag/        NewItemTypeDialog nitd = new NewItemTypeDialog();
//pag/        nitd.setParent(this);
//pag/        nitd.show(getFragmentManager(), "Diaggg");
    }

    public void BackupDatabase(String query, Object... params)
    {
        LocalDateTime curr = LocalDateTime.now();
        String tempName = String.format(Locale.US, "jrnl_%s_", curr.toString());

        try 
        {
            File sdcard = Environment.getExternalStorageDirectory();
            InputStream in = new FileInputStream(getDatabasePath("jrnl.db"));

            File tempFile = File.createTempFile(tempName, ".db", this.getExternalCacheDir());
            backupDatabaseName = "/storage/emulated/0/Android/data/org.giancola.jrnl/cache/" + tempFile.getName();
            tempFile.deleteOnExit();
            FileOutputStream out = new FileOutputStream(tempFile);

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();

            out.close();
            in.close();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }

    private String DumpDatabase()
    {
        String dump = new String();

        dump += "PRAGMA foreign_keys=OFF;\n";
        dump += "BEGIN TRANSACTION;\n";
        // Dump the events table.
        dump += "CREATE TABLE events (_id INTEGER PRIMARY KEY, date TEXT, time TEXT, itemid INTEGER, typeid INTEGER, size INTEGER);\n";
        Cursor c = m_db.rawQuery("select * from events;", null);
        if (c.moveToFirst()) 
        {
            while (!c.isAfterLast()) 
            {
                int id = c.getInt(0);
                String date = c.getString(1);
                String time = c.getString(2);
                int itemid = c.getInt(3);
                int typeid = c.getInt(4);
                int size = c.getInt(5);
                dump += String.format("INSERT INTO events VALUES(%d,\"%s\",\"%s\",%d,%d,%d);\n",
                    id, date, time, itemid, typeid, size);
                c.moveToNext();
            }
        }
        // Dump the events table.
        dump += "CREATE TABLE items (_id INTEGER PRIMARY KEY, name TEXT NOT NULL UNIQUE, usage INTEGER);\n";
        c = m_db.rawQuery("select * from items;", null);
        if (c.moveToFirst()) 
        {
            while (!c.isAfterLast()) 
            {
                int id = c.getInt(0);
                String name = c.getString(1);
                int usage = c.getInt(2);
                dump += String.format("INSERT INTO items VALUES(%d,\"%s\",%d);\n", id, name, usage);
                c.moveToNext();
            }
        }
        // Dump the types table.
        dump += "CREATE TABLE types (_id INTEGER PRIMARY KEY, name TEXT NOT NULL UNIQUE, usage INTEGER);\n";
        c = m_db.rawQuery("select * from types;", null);
        if (c.moveToFirst()) 
        {
            while (!c.isAfterLast()) 
            {
                int id = c.getInt(0);
                String name = c.getString(1);
                int usage = c.getInt(2);
                dump += String.format("INSERT INTO types VALUES(%d,\"%s\",%d);\n", id, name, usage);
                c.moveToNext();
            }
        }
        // Dump the sizes table.
        dump += "CREATE TABLE sizes (_id INTEGER PRIMARY KEY, size INTEGER);\n";
        c = m_db.rawQuery("select * from sizes;", null);
        if (c.moveToFirst()) 
        {
            while (!c.isAfterLast()) 
            {
                int id = c.getInt(0);
                int size = c.getInt(1);
                dump += String.format("INSERT INTO sizes VALUES(%d,%d);\n", id, size);
                c.moveToNext();
            }
        }
        // Dump the lastids table.
        dump += "CREATE TABLE lastids (_id INTEGER PRIMARY KEY, lastitem INTEGER, lasttype INTEGER, lastsize INTEGER, lastevent INTEGER);\n";
        c = m_db.rawQuery("select * from lastids;", null);
        if (c.moveToFirst()) 
        {
            while (!c.isAfterLast()) 
            {
                int id = c.getInt(0);
                int lastitem = c.getInt(1);
                int lasttype = c.getInt(2);
                int lastsize = c.getInt(3);
                int lastevent = c.getInt(4);
                dump += String.format("INSERT INTO lastids VALUES(%d,%d,%d,%d,%d);\n", id, lastitem, lasttype, lastsize, lastevent);
                c.moveToNext();
            }
        }
        dump += "COMMIT;\n";
        return dump;
    }

    public void EmailDatabase(String query, Object... params)
    {
        if (backupDatabaseName.length() == 0)
        {
            Toast.makeText(this, "No current backup database.", Toast.LENGTH_LONG).show();
            return;
        }
        try
        {
            File dbfile = new File(backupDatabaseName);
            String body = dbfile.getAbsolutePath() + "\n";
            body += DumpDatabase();
            URI uri = new URI(backupDatabaseName);
            Intent it = new Intent(Intent.ACTION_SEND);
            it.putExtra(Intent.EXTRA_EMAIL, new String[]{"peter.giancola@gmail.com"});
            it.putExtra(Intent.EXTRA_SUBJECT, "Database from Jrnl");
            it.putExtra(Intent.EXTRA_TEXT, body);
            it.setType("message/rfc822");
            it.setPackage("com.google.android.gm");
            startActivity(it);
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    public void SetNow(String query, Object... params)
    {
        GetNow(true, true);
    }

    public void GetDate(String query, Object... params)
    {
        GetNow(true, false);
    }

    public void GetTime(String query, Object... params)
    {
        GetNow(false, true);
    }

    public void PopulateItems(String query, Object... params)
    {
//pag/        String sqlquery = "select _id, name from items order by name;";
//pag/        itemCursor = m_db.rawQuery(sqlquery, null);
//pag/        String[] adapterCols = new String[]{"name"};
//pag/        int[] adapterRowViews = new int[]{android.R.id.text1};
//pag/        sca = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, 
//pag/                itemCursor, adapterCols, adapterRowViews, 0);
//pag/        itemSpinner.setAdapter(sca);
//pag/        itemSpinner.setOnItemSelectedListener(this);
    }

    public void PopulateTypes(String query, Object... params)
    {
//pag/        String sqlquery = "select _id, name from types order by name;";
//pag/        typeCursor = m_db.rawQuery(sqlquery, null);
//pag/        String[] adapterCols = new String[]{"name"};
//pag/        int[] adapterRowViews = new int[]{android.R.id.text1};
//pag/        sca = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, 
//pag/                typeCursor, adapterCols, adapterRowViews, 0);
//pag/        typeSpinner.setAdapter(sca);
//pag/        typeSpinner.setOnItemSelectedListener(this);
    }

    public void PopulateSizes(String query, Object... params)
    {
        // Need to get this list from the database just like the others.
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item, 
                sizeNames);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sizeSpinner.setAdapter(aa);
        sizeSpinner.setOnItemSelectedListener(this);
    }

    // Send the a message to the given state machine.
    public void execute(String msg, StateMachine sm)
    {
        if (sm == null)
        {
            tv.append("execute(): sm is null\n");
            return;
        }
        try
        {
            SMMatch ma = new SMMatch(msg);

            Log.d(LOGTAG, String.format(Locale.US, "--- Curr: %s, Input: %s", sm.getCurrState().getName(), msg));
            sm.input(this, ma);
            Log.d(LOGTAG, String.format(Locale.US, "---  New: %s", sm.getCurrState().getName()));
        } 
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    public class MyCursorAdapter extends CursorAdapter
    {
        private LayoutInflater linf;

        public MyCursorAdapter(Context context, Cursor cursor, int flags)
        {
            super(context, cursor, flags);
            linf = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent)
        {
            return linf.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor)
        {
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
    {
    }

    public void onNothingSelected(AdapterView<?> parent)
    {
    }

    private StateDiagram loadStateDiagram(int resourceID)
    {
        StateDiagram sd = null;
        // Read the state diagram from the file.
        InputStream fis = this.getResources().openRawResource(resourceID);
        try
        {
            sd = new StateDiagram();
            sd.fromXml(fis);
        } 
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
        return sd;
    }

    public void GetNow(Boolean date, Boolean time)
    {
        // ********************************************
        // Need to store result for subsequent actions.
        // ********************************************
        if (date)
        {
            Calendar c = Calendar.getInstance();
            int y = c.get(Calendar.YEAR);
            int m = c.get(Calendar.MONTH);
            int d = c.get(Calendar.DAY_OF_MONTH);
            // Calendar returns month with zero-origin, adjust.
            ++m;
            String lll = String.format(Locale.US, "%04d-%02d-%02d", y, m, d);
//pag/            tv.append("GetDate() " + lll + "\n");
            editDate.setText(lll);
        }
        if (time)
        {
            Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            int sec = c.get(Calendar.SECOND);
            double mins = ((double)minute * 100.0) + ((double)sec * 1.666667);
            minute = (int)RoundTo((mins / 100.0), (double)5.0, (int)0);
            // Check for overflow of minutes.
            if (minute >= 60)
            {
                minute -= 60;
                ++hour;
                // Check for overflow of hours.
                if (hour >= 24)
                {
                    tv.append("hour overflow\n");
                    // We're near midnight, just set 23:55.
                    hour = 23;
                    minute = 55;
                }
            }
            sec = 0;
            String lll = String.format(Locale.US, "%02d:%02d:%02d", hour, minute, sec);
//pag/            tv.append("GetTime() " + lll + "\n");
            editTime.setText(lll);
        }
    }

    public Boolean ValidateDateTime(DateTimeHolder dth)
    {
        String[] dates = dth.getDate().split("-");
        String[] times = dth.getTime().split(":");

        int year = Integer.parseInt(dates[0]);
        int month = Integer.parseInt(dates[1]);
        int day = Integer.parseInt(dates[2]);
        int hour = Integer.parseInt(times[0]);
        int minute = Integer.parseInt(times[1]);
        int second = Integer.parseInt(times[2]);
        int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        // Handle leap years.
        if ((year % 4) == 0)
        {
            if ((year % 100) == 0)
            {
                if ((year % 400) == 0)
                {
                    days[1] = 29;
                }
            }
            else
            {
                days[1] = 29;
            }
        }

        if (year <= 2008)
        {
            Toast.makeText(this, "Year <= 2008", Toast.LENGTH_LONG).show();
            return false;
        }
        if ((month < 0) || (12 < month))
        {
            Toast.makeText(this, "Month not 1 - 12", Toast.LENGTH_LONG).show();
            return false;
        }
        if ((day < 0) || (days[month] < day))
        {
            Toast.makeText(this, String.format(Locale.US, "Day not 1 - %d", 
                days[month]), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((hour < 0) || (23 < hour))
        {
            Toast.makeText(this, "Hour not 0 - 23", Toast.LENGTH_LONG).show();
            return false;
        }
        if ((minute < 0) || (59 < minute))
        {
            Toast.makeText(this, "Minute not 0 - 59", Toast.LENGTH_LONG).show();
            return false;
        }
        if ((second < 0) || (59 < second))
        {
            Toast.makeText(this, "Second not 0 - 59", Toast.LENGTH_LONG).show();
            return false;
        }

        // Return the reformatted date and time.
        dth.setDate(String.format(Locale.US, "%04d-%02d-%02d", year, month, day));
        dth.setTime(String.format(Locale.US, "%02d:%02d:%02d", hour, minute, second));
        return true;
    }

    // Load the state diagrams into the app's state machines.
    private void initStateMachines()
    {
        m_appsm = new StateMachine(loadStateDiagram(R.raw.jrnlappsd));
        m_sm = new StateMachine(loadStateDiagram(R.raw.jrnlsd));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initStateMachines();
        execute("Begin", m_sm);
        execute("onCreate", m_appsm);
    }
    // Catch the app state changes.
    @Override protected void onStart() { super.onStart(); execute("onStart", m_appsm); }
    @Override protected void onPause() { super.onPause(); execute("onPause", m_appsm); }
    @Override protected void onResume() { super.onResume(); execute("onResume", m_appsm); }
    @Override protected void onRestart() { super.onRestart(); execute("onRestart", m_appsm); }
    @Override protected void onStop() { super.onStop(); execute("onStop", m_appsm); }
    @Override protected void onDestroy() { super.onDestroy(); execute("onDestroy", m_appsm); }
    // Catch activity events.
    @Override public void onClick(View view)
    {
        Button button = (Button)view; 
        Log.d(LOGTAG, "Click: " + button.getText().toString());
        execute("Click" + button.getText().toString(), m_sm);
    }
 
}

/*








2018-12-22 18:43:23.806 12821-12821/? D/Peep: onFocusChange: FocusChangeItemEditTextIn
2018-12-22 21:05:12.163 13622-13622/org.giancola.jrnl D/Peep: --- Curr: Init, Input: Begin
2018-12-22 21:05:12.823 13622-13622/org.giancola.jrnl D/Peep: ---  New: Running
2018-12-22 21:05:12.823 13622-13622/org.giancola.jrnl D/Peep: --- Curr: Init, Input: onCreate
2018-12-22 21:05:12.826 13622-13622/org.giancola.jrnl D/Peep: ---  New: Creating
2018-12-22 21:05:12.831 13622-13622/org.giancola.jrnl D/Peep: --- Curr: Creating, Input: onStart
2018-12-22 21:05:12.832 13622-13622/org.giancola.jrnl D/Peep: ---  New: Starting
2018-12-22 21:05:12.838 13622-13622/org.giancola.jrnl D/Peep: --- Curr: Starting, Input: onResume
2018-12-22 21:05:12.839 13622-13622/org.giancola.jrnl D/Peep: ---  New: Starting
2018-12-22 21:05:13.364 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeTypeEditTextIn
2018-12-22 21:05:13.365 13622-13622/org.giancola.jrnl D/Peep: --- Curr: Running, Input: FocusChangeTypeEditTextIn
2018-12-22 21:05:13.380 13622-13622/org.giancola.jrnl D/Peep: ---  New: TypeView
2018-12-23 10:30:16.125 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeTypeEditTextOut
2018-12-23 10:30:16.126 13622-13622/org.giancola.jrnl D/Peep: --- Curr: TypeView, Input: FocusChangeTypeEditTextOut
2018-12-23 10:30:16.128 13622-13622/org.giancola.jrnl D/Peep: ---  New: Running
2018-12-23 10:30:16.129 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeItemEditTextIn
2018-12-23 10:30:16.130 13622-13622/org.giancola.jrnl D/Peep: --- Curr: Running, Input: FocusChangeItemEditTextIn
2018-12-23 10:30:16.148 13622-13622/org.giancola.jrnl D/Peep: ---  New: ItemEdit
2018-12-23 10:30:20.305 13622-13622/org.giancola.jrnl D/Peep: --- Curr: ItemEdit, Input: TextChangedItemEditText
2018-12-23 10:30:20.387 13622-13622/org.giancola.jrnl D/Peep: ---  New: ItemEdit
2018-12-23 10:30:21.925 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeItemEditTextOut
2018-12-23 10:30:21.925 13622-13622/org.giancola.jrnl D/Peep: --- Curr: ItemEdit, Input: FocusChangeItemEditTextOut
2018-12-23 10:30:21.926 13622-13622/org.giancola.jrnl D/Peep: ---  New: ItemEditOut
2018-12-23 10:30:21.927 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeItemListViewIn
2018-12-23 10:30:21.927 13622-13622/org.giancola.jrnl D/Peep: --- Curr: ItemEditOut, Input: FocusChangeItemListViewIn
2018-12-23 10:30:21.928 13622-13622/org.giancola.jrnl D/Peep: ---  New: ItemListView
2018-12-23 10:30:33.510 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeItemListViewOut
2018-12-23 10:30:33.510 13622-13622/org.giancola.jrnl D/Peep: --- Curr: ItemListView, Input: FocusChangeItemListViewOut
2018-12-23 10:30:33.521 13622-13622/org.giancola.jrnl D/Peep: onFocusChange: FocusChangeItemEditTextIn



*/
