package org.giancola.jrnl;

import android.app.Dialog;
import android.app.DialogFragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import javax.xml.parsers.ParserConfigurationException;
import org.giancola.statemachine.SMMatch;
import org.giancola.statemachine.StateDiagram;
import org.giancola.statemachine.StateMachine;
import org.xml.sax.SAXException;

public class NewItemTypeDialog extends DialogFragment
{
    private StateMachine m_sm = null;
    private EditText newItem = null;
    private EditText newType = null;
    private TextView tv = null;

    private SQLiteDatabase m_db = null;
    private MainActivity m_parent = null;

    private String m_item = "";
    private String m_type = "";

    public void setParent(MainActivity parent)
    {
        m_parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
    {
        m_db = m_parent.getdb();

        m_sm = new StateMachine(loadStateDiagram(R.raw.jrnlitem));
        execute("Begin", m_sm);

        LinearLayout mainLayout = new LinearLayout(getActivity());
        mainLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout itemLayout = new LinearLayout(getActivity());
        itemLayout.setOrientation(LinearLayout.HORIZONTAL);

        TextView newItemTxt = new TextView(getActivity());
        newItemTxt.setText("Item:");
        newItem = new EditText(getActivity());
        newItem.setWidth(300);
        newItem.addTextChangedListener(new TextWatcher() 
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) 
                {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) 
                {
                }

                @Override
                public void afterTextChanged(Editable s) 
                {
                    tv.setText("");
                    m_item = s.toString();
                    String query = String.format("select name from items where name like '%%%s%%';", m_item);
                    Cursor c = m_db.rawQuery(query, null);
                    if (c.moveToFirst())
                    {
                        while (!c.isAfterLast())
                        {
                            String name = c.getString(0);
                            tv.append(name + "\n");
                            c.moveToNext();
                        }
                    }
                }
            });
        itemLayout.addView(newItemTxt);
        itemLayout.addView(newItem);

        LinearLayout typeLayout = new LinearLayout(getActivity());
        typeLayout.setOrientation(LinearLayout.HORIZONTAL);

        TextView newTypeTxt = new TextView(getActivity());
        newTypeTxt.setText("Type:");
        newType = new EditText(getActivity());
        newType.setWidth(300);
        typeLayout.addView(newTypeTxt);
        typeLayout.addView(newType);


        Button b = new Button(getActivity());
        b.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    Button button = (Button)v;
                    execute("Click" + button.getText().toString(), m_sm);
                }
            });
        b.setText("New");

        mainLayout.addView(itemLayout);
        mainLayout.addView(typeLayout);
        mainLayout.addView(b);

        tv = new TextView(getActivity());
        tv.setWidth(600);
//pag/        tv.setHeight(1000);

        mainLayout.addView(tv);
    
        return mainLayout;
    }

    public void SetNew(String query, Object... params)
    {
        Toast.makeText(m_parent, "New!", Toast.LENGTH_LONG).show();
    }

    public void execute(String msg, StateMachine sm)
    {
        if (sm == null)
        {
            return;
        }
        try
        {
            SMMatch ma = new SMMatch(msg);
            sm.input(this, ma);
        } 
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    private StateDiagram loadStateDiagram(int resourceID)
    {
        StateDiagram sd = null;
        // Read the state diagram from the file.
        InputStream fis = this.getResources().openRawResource(resourceID);
        try
        {
            sd = new StateDiagram();
            sd.fromXml(fis);
        } 
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
        return sd;
    }

}
