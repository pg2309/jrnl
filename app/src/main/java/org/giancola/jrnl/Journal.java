package org.giancola.jrnl;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.*;

/**
 * Created by pagianc on 11/10/2018.
 */
public class Journal extends SQLiteOpenHelper 
{

    public static final Integer VERSION = 1;
    public Context m_context = null;
    public int resourceID;

    public Journal(Context context, String DBNAME, int resid)
    {
        super(context, DBNAME, null, VERSION);
        resourceID = resid;
        context.deleteDatabase(DBNAME);
        m_context = context;
    }

    public boolean initDb(SQLiteDatabase db)
    {
        boolean success = true;

        try
        {
            InputStream is = m_context.getResources().openRawResource(resourceID);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = null;

            line = reader.readLine();
            while (line != null)
            {
                db.execSQL(line);
                line = reader.readLine();
            }
            is.close();
        }
        catch (Exception e)
        {
            success = false;
        }

        return success;
    }

    public Integer queryInt(SQLiteDatabase db, String query)
    {
        Integer val = -1;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst())
        {
            val = Integer.parseInt(cursor.getString(0));
        }
        cursor.close();
        return val;
    }

    public Integer getMaxID(SQLiteDatabase db, String idField, String tableName)
    {
        Integer maxId = 0;
        String qForMax = "select " + idField + " from " + tableName + " order by " + idField + " desc limit 1;";

        maxId = queryInt(db, qForMax);
        if (maxId == -1)
        {
            maxId = 0;
        }
        return maxId;
    }

    public Integer getCategoryID(SQLiteDatabase db, String name)
    {
        String query = "select CTID from category where Name = \"" + name + "\";";
        return queryInt(db, query);
    }

    public Integer addCategory(SQLiteDatabase db, String name)
    {
        Integer id = getCategoryID(db, name);
        if (id == -1)
        {
            id = getMaxID(db, "CTID", "category");
            ++id;
            db.execSQL("insert into category values (" + id.toString() + ", \"" + name + "\");");
        }
        return id;
    }

    public Integer getTypeID(SQLiteDatabase db, String name)
    {
        String query = "select TYID from type where Name = \"" + name + "\";";
        return queryInt(db, query);
    }

    public Integer addType(SQLiteDatabase db, String name)
    {
        Integer id = getTypeID(db, name);
        if (id == -1)
        {
            id = getMaxID(db, "TYID", "type");
            ++id;
            db.execSQL("insert into type values (" + id.toString() + ", \"" + name + "\");");
        }
        return id;
    }

    public Integer getItemByTypeCategory(SQLiteDatabase db, Integer tyid, Integer ctid)
    {
        Integer itid = -1;

        if ((tyid >= 0) && (ctid >= 0))
        {
            String query = "select ITID from item where CTID = " + ctid.toString() + " and TYID = " + tyid.toString() + ";";
            itid = queryInt(db, query);
        }
        return itid;
    }

    public Integer getItemByTypeCategory(SQLiteDatabase db, String type, String category)
    {
        Integer tyid = getTypeID(db, type);
        Integer ctid = getCategoryID(db, category);
        return getItemByTypeCategory(db, tyid, ctid);
    }

    public Integer getItemByTypeCategory(SQLiteDatabase db, Integer tyid, String category)
    {
        Integer ctid = getCategoryID(db, category);
        return getItemByTypeCategory(db, tyid, ctid);
    }

    public Integer getItemByTypeCategory(SQLiteDatabase db, String type, Integer ctid)
    {
        Integer tyid = getTypeID(db, type);
        return getItemByTypeCategory(db, tyid, ctid);
    }

    public Integer addItem(SQLiteDatabase db, String type, String category)
    {
        Integer itid = getItemByTypeCategory(db, type, category);
        if (itid == -1)
        {
            // Didn't find an existing item, get the last used type ID and make
            // a new entry with the next ID.
            itid = getMaxID(db, "ITID", "item");
            ++itid;
            Integer tyid = addType(db, type);
            Integer ctid = addCategory(db, category);
            db.execSQL("insert into items values (" + itid + ", " + ctid + ", " + tyid + ", 0, 0);");
        }
        return itid;
    }

    public Integer addItem(SQLiteDatabase db, Integer tyid, String category)
    {
        Integer itid = getItemByTypeCategory(db, tyid, category);
        if (itid == -1)
        {
            // Didn't find an existing item, get the last used type ID and make
            // a new entry with the next ID.
            itid = getMaxID(db, "ITID", "item");
            ++itid;
            Integer ctid = addCategory(db, category);
            db.execSQL("insert into items values (" + itid + ", " + ctid + ", " + tyid + ", 0, 0);");
        }
        return itid;
    }

    public Integer addItem(SQLiteDatabase db, String type, Integer ctid)
    {
        Integer itid = getItemByTypeCategory(db, type, ctid);
        if (itid == -1)
        {
            // Didn't find an existing item, get the last used type ID and make
            // a new entry with the next ID.
            itid = getMaxID(db, "ITID", "item");
            ++itid;
            Integer tyid = addType(db, type);
            db.execSQL("insert into items values (" + itid + ", " + ctid + ", " + tyid + ", 0, 0);");
        }
        return itid;
    }

    public Integer addItem(SQLiteDatabase db, Integer tyid, Integer ctid)
    {
        Integer itid = getItemByTypeCategory(db, tyid, ctid);
        if (itid == -1)
        {
            // Didn't find an existing item, get the last used type ID and make
            // a new entry with the next ID.
            itid = getMaxID(db, "ITID", "item");
            ++itid;
            db.execSQL("insert into items values (" + itid + ", " + ctid + ", " + tyid + ", 0, 0);");
        }
        return itid;
    }

    @Override
    public void onCreate(SQLiteDatabase db) 
    {
        boolean succ = initDb(db);
    } 

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
    }
}
